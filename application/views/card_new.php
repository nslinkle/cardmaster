<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>카드 추가<small>_</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid"><form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="span4">
                            <h2>이름</h2>
                            <div class="input-control text span4">
                              <input type="text" name="name"/>
                            </div>
                        </div>
                        <div class="span4">
                            <h2>이미지</h2>
                            <div class="input-control file span4">
                              <input type="file" name="image"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>설명</h2>
                            <div class="input-control textarea span8">
                              <textarea name="description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>종류</h2>
                            <div class="input-control select span8">
                              <select name="type"><?foreach($card_type as $type) {?>
                                <option value="<?=$type['card_type_id']?>"><?=$type['name']?></option><?}?>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>기능</h2><?foreach($card_brand as $brand) {?>
                            <label class="input-control checkbox">
                                <input type="checkbox" name="brand[<?=$brand['card_brand_id']?>]" value="1">
                                <span class="helper"><?=$brand['name']?></span>
                            </label><?}?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <input type="submit" value="등록">
                        </div>
                    </div>
                </form></div>
            </div>
        </div>
    </div>

<? include("footer.php")?>
