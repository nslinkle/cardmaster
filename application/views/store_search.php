<? include("header.php")?>

    <div class="page secondary with-sidebar">
        <div class="page-header">
            <div class="page-header-content">
<?
if(isset($query)) { ?>
                <h1>가맹점 검색<small>"<?=$query?>"에 대한 검색 결과</small></h1>
<?
} else { ?>
                <h1>가맹점 검색<small>목록 보기</small></h1>
<?
} ?>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-sidebar" style="margin-bottom: 10px;">
            <ul>
                <li class="sticker sticker-color-red">
                    <a href="?">모두 보기</a>
                </li>
                <li class="sticker sticker-color-blue">
                    <a>혜택별 보기</a>
                    <ul class="sub-menu light">
<?
foreach($store_benefit_type_list as $type) {
    if($typeid == $type['store_benefit_type_id']) { ?>
                        <li style="background-color: #8F8F8F;"><a href="?typeid=<?=$type['store_benefit_type_id']?>"><b><?=$type['name']?></b></a></li>
<?
    } else { ?>
                        <li><a href="?typeid=<?=$type['store_benefit_type_id']?>"><?=$type['name']?></a></li>
<?
    }
} ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid" style="position: absolute;">
                    <div class="row">
                        <div class="span9"><h2>검색</h2></div>
                    </div>
                    <form method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="input-control text span7" style="padding-right:10px;">
<?
if(isset($query)) { ?>
                              <input type="text" name="query" value="<?=$query?>" />
<?
} else { ?>
                              <input type="text" name="query" />
<?
} ?>
                            </div>
                            <div class="input-control select span2">
                              <select name="search_type">
                                <option value="name">이름</option>
                              </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span9">
                                <input type="submit" value="검색">
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        
                    </div>
                    <div class="row">
                        <div class="span9"><h2>가맹점 목록</h2></div>
                    </div>
<?
if(count($store_list) == 0) { ?>
                    <div class="tile quadro tile bg-color-red">
                        <div class="tile-content">
                            <h4 style="margin-bottom: 5px;">결과 없음</h4>
                            <p>조건에 해당하는 결과가 없습니다.</p>
                        </div>
                        <div class="brand">
                            <div class="badge attention"></div>
                        </div>
                    </div>
<?
} else {
    foreach($store_list as $store) { ?>
                    <a href="/store/<?=$store['id']?>"><div class="tile quadro">
                        <div class="tile-content">
<?      if($store['image']) { ?>
                            <img width="80px" src="/static/upload/<?=$store['image']?>" class="place-left">
<?      } ?>
                            <h4 style="margin-bottom: 5px;"><?=$store['name']?></h4>
                            <p><?=$store['description']?></p>
                        </div>
                        <div class="brand">
                            <span class="name"><?=$store['type']?></span>
                        </div>
                    </div></a>
<?  }
} ?>
                </div>
            </div>
        </div>
    </div>
    <style>
#footer{
  display: none;
}
    </style>

<? include("footer.php")?>
