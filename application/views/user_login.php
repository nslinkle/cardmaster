<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>로그인<small>서비스를 이용하시려면 로그인해주세요.</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
<?
if(isset($result) && $result == false) {
?>
                <div class="tile quadro tile bg-color-red">
                    <div class="tile-content">
                        <h4 style="margin-bottom: 5px;">로그인 실패</h4>
<?
    foreach($message as $msg) {
?>
                        <p><?=$msg?></p>
<?
    }
?>
                    </div>
                    <div class="brand">
                        <div class="badge attention"></div>
                    </div>
                </div>
<?
} else if(isset($result) && $result == true) {
?>
                <div class="tile quadro tile bg-color-green">
                    <div class="tile-content">
                        <h4 style="margin-bottom: 5px;">회원가입에 성공했습니다.</h4>
                        <p>아래 페이지를 통해 로그인해주세요.</p>
                    </div>
                    <div class="brand">
                        <div class="badge attention"></div>
                    </div>
                </div>
<?
}
?>
                <div class="grid"><form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="span4">
                            <h2>이메일</h2>
                            <div class="input-control text span4">
                              <input type="text" name="email"/>
                            </div>
                        </div>
                        <div class="span6">
                            <h2>비밀번호</h2>
                            <div class="input-control text span4">
                              <input type="password" name="password"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <input type="submit" value="로그인">
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>가입하지 않으셨나요?</h2>
                            <p><a href="/user/signup">회원가입</a>을 하시거나 <a href="/login/facebook">Facebook 계정으로 로그인</a>하실 수 있습니다.</p>
                        </div>
                    </div>
                </form></div>
            </div>
        </div>
    </div>

<? include("footer.php")?>
