<? include("header.php")?>

<?
if(isset($store['name'])) {
?>
    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1><?=$store['name']?><small><?=$store['type']?></small></h1>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid">
                    <div class="row">
                        <div class="span10">
                            <h2>로고</h2>
<?
if($store['image'] != "") {
?>
                            <p><img src="/static/upload/<?=$store['image']?>"></p>
<?
}
?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>정보</h2>
                            <p><?=$store['description']?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>제공 혜택</h2>
                            <form method="post"><div style="width:630px; height:65px; margin-bottom:10px; padding:20px; background-color:#FFC40D;">
                                <div class="input-control text span2">
                                  <input name="price" type="text" value="<?=$benefit_price?>"/>
                                </div>
                                <span style="line-height:30px;">원을 결제할 때 할인율이 높은 순서</span>
                            <input type="submit" value="정렬" style="float:right">
                            </div></form>
<?foreach($store_benefit as $benefit) {?>
                            <a href="/card/<?=$benefit['card_id']?>"><div class="tile quadro">
                                <div class="tile-content">
<?      if($benefit['card_image']) { ?>
                                    <img width="180px" src="/static/upload/<?=$benefit['card_image']?>" class="place-left">
<?      } ?>
                                    <h4 style="margin-bottom: 5px;"><?=$benefit['card_name']?></h4>
                                    <p><?if($benefit['benefit_rate']!=0){echo ($benefit['benefit_rate']*100).'% ';}else{echo $benefit['benefit_price'].'원 ';} if($benefit['benefit_type']==1){echo '할인';}else{echo '적립';}?></p>
                                    <p><?=$benefit['discount']?>원 할인</p>
                                    <p><?=$benefit['benefit_description']?></p>
                                </div>
                                <div class="brand">
                                </div>
                            </div></a>
<?}?>
                            <div class="tile quadro" onclick="popup('/benefit/storenew/<?=$store['id']?>');" style="cursor: pointer;">
                              <div class="tile-content">
                                 <h2><i class="icon-plus-2"></i>새 혜택 추가</h2>
                              </div>
                              <div class="brand">
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
} else {
?>
    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>잘못된 접근<small>오류</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
    </div>
<?
}
?>

<? include("footer.php")?>
