<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>내 카드<small>_</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid">
<?foreach($card_list as $card) {?>
                    <div class="row"><a href="/card/<?=$card['card_id']?>" style="color:#ffffff;">
                        <div class="span3"><?if($card['image']) {?>
                            <img src="/static/upload/<?=$card['image']?>" width="100%"><?}?>
                        </div>
                        <div class="span6 bg-color-<?if($card['usable']){?>green<?}else{?>grayDark<?}?> padding20">
                            <h2 style="color:#ffffff;"><?=$card['name']?><small><?=$card['type']?></small></h2>
                            <p><?=$card['description']?></p><?foreach($card['brand'] as $brand) { if($brand['image']){?>
                            <img src="/static/upload/<?=$brand['image']?>"><?}else {?>
                            <span><?=$brand['name']?></span><?}?>
                            <?}?>
                        </div></a>
                        <div class="span1">
<?if($card['usable']){?>
                             <a href="/user/cardusable/<?=$card['card_id']?>/0" class="button">비활성</a>
<?}else{?>
                             <a href="/user/cardusable/<?=$card['card_id']?>/1" class="button bg-color-green">활성</a>
<?}?>
                             <a href="/user/carddelete/<?=$card['card_id']?>"class="button bg-color-red">삭제</a>
                        </div>
                    </div><?}?>
                </div>
            </div>
        </div>
    </div>

<? include("footer.php")?>
