<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1><?=$card_info['name']?><small><?=$card_info['type']?></small></h1>
                <a href="javascript:history.back()" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid">
                    <div class="row">
                        <div class="span10">
<?if($card_info['image']) {?>
                            <p><img src="/static/upload/<?=$card_info['image']?>" height="200px"></p><?}?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>정보</h2>
                            <p><?=$card_info['description']?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>브랜드</h2>
                            <p><?foreach($card_info['brand'] as $brand) { if($brand['image']){?>
                            <img src="/static/upload/<?=$brand['image']?>"><?}else {?>
                            <span><?=$brand['name']?></span><?}?>
                            <?}?></p>
                        </div>
                    </div>
                    <div class="row"><h2>혜택</h2>
<?foreach($card_benefit as $benefit) {?>
                    <a href="/store/<?=$benefit['store_id']?>"><div class="tile double">
                        <div class="tile-content">
<?      if($benefit['store_image']) { ?>
                            <img width="80px" src="/static/upload/<?=$benefit['store_image']?>" class="place-left">
<?      } ?>
                            <h4 style="margin-bottom: 5px;"><?=$benefit['store_name']?></h4>
                            <p><?if($benefit['benefit_rate']!=0){echo ($benefit['benefit_rate']*100).'% ';}else{echo $benefit['benefit_price'].'원 ';} if($benefit['benefit_type']==1){echo '할인';}else{echo '적립';}?></p>
                            <p><?=$benefit['benefit_description']?></p>
                        </div>
                    </div></a>
<?}?>
                    <div class="tile double" onclick="popup('/benefit/cardnew/<?=$card_info['card_id']?>');" style="cursor: pointer;">
                      <div class="tile-content">
                         <h2><i class="icon-plus-2"></i>새 혜택 추가</h2>
                      </div>
                      <div class="brand">
                      </div>
                    </div>
                    <a href="/user/newcard/<?=$card_info['card_id']?>"><div class="tile double">
                      <div class="tile-content">
                         <h2><i class="icon-plus-2"></i>내 카드로 추가</h2>
                      </div>
                      <div class="brand">
                      </div>
                    </div></a>
                </div>
            </div>
        </div>
    </div>


<? include("footer.php")?>
