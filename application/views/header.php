<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="<?=site_url('/static/css/modern.css')?>" rel="stylesheet">
    <link href="<?=site_url('/static/css/modern-responsive.css')?>" rel="stylesheet">
    <link href="<?=site_url('/static/css/site.css')?>" rel="stylesheet" type="text/css">
    <link href="<?=site_url('/static/js/google-code-prettify/prettify.css')?>" rel="stylesheet" type="text/css">

    <title>Card Master</title>
    <script>
function popup(url){ 
  window.open(url, "benefit_new", "width=800,height=600,history=no,resizable=no,status=no,scrollbars=yes,menubar=no")
}
    </script>
</head>
<body class="modern-ui" onload="prettyPrint()">

<? include("navigation.php")?>
