<? include("header.php")?>

<div class="page">
    <div class="page-region">
        <div class="page-region-content">
            <div class="grid">
                <div class="row">
                    <div class="span8">
                        <div class="hero-unit">
                            <div id="carousel1" class="carousel" data-role="carousel" data-param-duration="300">
                                <div class="slides">

                                    <div class="slide" id="slide2">
                                        <h2 class="fg-color-darken">서비스 소개</h2>
                                        <p class="bg-color-pink padding20 fg-color-white">
                                            여러장의 카드를 관리하고, 가장 유용한 혜택을 제공하는 카드를 찾아주는 서비스 입니다.
                                        </p>

                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> 카드 관리</li>
                                                <li><i class="icon-checkmark"></i> 카드 검색</li>
                                                <li><i class="icon-checkmark"></i> 카드 혜택 조회</li>
                                            </ul>
                                        </div>
                                        <div class="span3 place-left">
                                            <ul class="unstyled sprite-details">
                                                <li><i class="icon-checkmark"></i> 유용한 카드 추천</li>
                                                <li><i class="icon-checkmark"></i> 가맹점 혜택 조회</li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="span4 bg-color-blueLight" id="serviceStatistics">
                            <h2 class="">이용통계</h2>
                            <span class="name">사용자</span><span class="value"><?=$user_num?>명</span><br>
                            <span class="name">email회원</span><span class="value"><?=$user_email_num?>명</span><br>
                            <span class="name">facebook회원</span><span class="value"><?=$user_facebook_num?>명</span><br>
                            <span class="name">등록된 카드</span><span class="value"><?=$card_num?>장</span><br>
                            <span class="name">등록된 가맹점</span><span class="value"><?=$store_num?>개</span><br>
                            <span class="name">등록된 혜택</span><span class="value"><?=$benefit_num?>개</span><br>
                            <span class="name">검색 횟수</span><span class="value">00회</span><br>
                        </div>
                    </div>
                </div>
            </div>

<?if(isset($rec_card)){?>
            <div class="grid">
                <div class="row">
                    <div class="span6 bg-color-blue">
                        <h2 class="fg-color-white">&nbsp;<i class="icon-credit-card"></i>&nbsp;카드 추천</h2>
                    </div>

                    <div class="span6 bg-color-green">
                        <h2 class="fg-color-white">&nbsp;<i class="icon-basket"></i>&nbsp;가맹점 추천</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="span6">
                    <?foreach($rec_card as $card){?>
                        <div>
                          <h2><?=$card['name']?></h2>
                          <?if($card['image']) {?><img width="150px;" src="/static/upload/<?=$card['image']?>" width="100%"><?}?>
                          <p><?=$card['description']?></p>
                          <p>총 <?=$card['td']?>원의 할인혜택이 제공되었습니다.</p>
                        </div>
                    <?}?>
                    </div>

                    <div class="span6">
                    <?foreach($rec_store as $store){?>
                        <div>
                          <h2><?=$store['name']?></h2>
                          <?if($store['image']) {?><img width="150px;" src="/static/upload/<?=$store['image']?>" width="100%"><?}?>
                          <p><?=$store['description']?></p>
                          <p>총 <?=$store['td']?>원의 할인혜택이 제공되었습니다.</p>
                        </div>
                    <?}?>
                    </div>
                </div>
            </div>
<?}?>

        </div>
    </div>
</div>

<? include("footer.php")?>
