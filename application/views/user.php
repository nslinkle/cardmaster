<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>내 정보<small>_</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>

        <div class="page-region">
            <div class="page-region-content">
                <div class="grid">
                    <div class="row">
                        <div class="span10">
                            <h2>이름</h2>
                            <div class="input-control text span4">
                              <input type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>성별</h2>
                            <label class="input-control radio" onclick="">
                                <input type="radio" name="r1"  checked=""/>
                                <span class="helper">남자</span>
                            </label>

                            <label class="input-control radio" onclick="">
                                <input type="radio" name="r1" />
                                <span class="helper">여자</span>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>나이</h2>
                            <div class="input-control text span4">
                              <input type="number" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <input type="submit" value="수정">
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

<? include("footer.php")?>
