<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>회원가입<small>이메일로 가입</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>

        <div class="page-region">
            <div class="page-region-content">
<?
if(isset($message)) {
?>
                <div class="tile quadro tile bg-color-red">
                    <div class="tile-content">
                        <h4 style="margin-bottom: 5px;">회원가입 실패</h4>
<?
    foreach($message as $msg) {
?>
                        <p><?=$msg?></p>
<?
    }
?>
                    </div>
                    <div class="brand">
                        <div class="badge attention"></div>
                    </div>
                </div>
<?
}
?>
                <div class="grid"><form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="span4">
                            <h2>이메일</h2>
                            <div class="input-control text span4">
                              <input type="text" name="email"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <h2>비밀번호</h2>
                            <div class="input-control text span4">
                              <input type="password" name="password"/>
                            </div>
                        </div>
                        <div class="span4">
                            <h2>비밀번호 확인</h2>
                            <div class="input-control text span4">
                              <input type="password" name="password2"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span4">
                            <h2>이름</h2>
                            <div class="input-control text span4">
                              <input type="text" name="name"/>
                            </div>
                        </div>
                        <div class="span2">
                            <h2>나이</h2>
                            <div class="input-control text span2">
                              <input type="number" name="age"/>
                            </div>
                        </div>
                        <div class="span2" style="line-height:31px;">
                            <h2>성별</h2>
                            <label class="input-control radio" onclick="">
                                <input type="radio" name="gender_male"  checked="true"/>
                                <span class="helper">남자</span>
                            </label>

                            <label class="input-control radio" onclick="">
                                <input type="radio" name="gender_female" />
                                <span class="helper">여자</span>
                            </label>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="span10">
                            <input type="submit" value="회원가입">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? include("footer.php")?>
