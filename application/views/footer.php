    <div id="footer" class="page">
        <div class="nav-bar">
            <div class="nav-bar-inner padding10">
                <span class="element">
                    2012, Software Systems Lab. Yeonjun Lim, Kiwook Ahn.
                </span>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?=site_url('/static/js/assets/jquery-1.8.2.min.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/assets/jquery.mousewheel.min.js')?>"></script>

    <script type="text/javascript" src="<?=site_url('/static/js/modern/dropdown.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/accordion.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/buttonset.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/carousel.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/input-control.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/pagecontrol.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/rating.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/slider.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/tile-slider.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/modern/tile-drag.js')?>"></script>

    <script type="text/javascript" src="<?=site_url('/static/js/assets/github.info.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/assets/google-analytics.js')?>"></script>
    <script type="text/javascript" src="<?=site_url('/static/js/google-code-prettify/prettify.js')?>"></script>
    <script src="<?=site_url('/static/js/sharrre/jquery.sharrre-1.3.4.min.js')?>"></script>

    <script>
        $('#shareme').sharrre({
            share: {
                googlePlus: true
                ,facebook: true
                ,twitter: true
                ,delicious: true
            },
            urlCurl: "<?=site_url('/static/js/sharrre/sharrre.php')?>",
            buttons: {
                googlePlus: {size: 'tall'},
                facebook: {layout: 'box_count'},
                twitter: {count: 'vertical'},
                delicious: {size: 'tall'}
            },
            hover: function(api, options){
                $(api.element).find('.buttons').show();
            }
        });
    </script>

    </body>
</html>
