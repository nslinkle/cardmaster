<? include("header.php")?>

    <div class="page secondary">
        <div class="page-header">
            <div class="page-header-content">
                <h1>카드 검색<small>_</small></h1>
                <a href="/" class="back-button big page-back"></a>
            </div>
        </div>
        <div class="page-region">
            <div class="page-region-content">
                <div class="grid">
                    <form method="post"><div class="row">
                        <div class="span4">
                            <h2>검색</h2>
                            <!--div class="input-control select span4">
                              <select>
                                <option>전체</option>
                                <option>이름</option>
                                <option>브렌드</option>
                              </select>
                            </div-->
                            <div class="input-control text span8">
                              <input name="q" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <input type="submit" value="검색">
                        </div>
                    </div></form><?foreach($card_list as $card) {?>
                    <div class="row"><a href="/card/<?=$card['card_id']?>" style="color:#ffffff;">
                        <div class="span3"><?if($card['image']) {?>
                            <img src="/static/upload/<?=$card['image']?>" width="100%"><?}?>
                        </div>
                        <div class="span7 bg-color-green padding20">
                            <h2 style="color:#ffffff;"><?=$card['name']?><small><?=$card['type']?></small></h2>
                            <p><?=$card['description']?></p><?foreach($card['brand'] as $brand) { if($brand['image']){?>
                            <img src="/static/upload/<?=$brand['image']?>"><?}else {?>
                            <span><?=$brand['name']?></span><?}?>
                            <?}?>
                        </div>
                    </a></div><?}?>
                </div>
            </div>
        </div>
    </div>

<? include("footer.php")?>
