
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="description" content="Metro UI CSS">
    <meta name="author" content="Sergey Pimenov">
    <meta name="keywords" content="windows 8, modern style, Metro UI, style, modern, css, framework">

    <link href="http://cardmaster.lkl.kr/static/css/modern.css" rel="stylesheet">
    <link href="http://cardmaster.lkl.kr/static/css/site.css" rel="stylesheet" type="text/css">
    <link href="http://cardmaster.lkl.kr/static/js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/static/css/token-input-facebook.css" type="text/css" />

    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/assets/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/assets/jquery.mousewheel.min.js"></script>

    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/dropdown.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/accordion.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/buttonset.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/carousel.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/input-control.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/pagecontrol.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/rating.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/slider.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/tile-slider.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/modern/tile-drag.js"></script>

    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/assets/github.info.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/assets/google-analytics.js"></script>
    <script type="text/javascript" src="http://cardmaster.lkl.kr/static/js/google-code-prettify/prettify.js"></script>
    <script src="http://cardmaster.lkl.kr/static/js/sharrre/jquery.sharrre-1.3.4.min.js"></script>

    <script type="text/javascript" src="/static/js/jquery.tokeninput.js"></script>
    <script type="text/javascript"> 
    $(function() {
        $("#card").tokenInput("/api/cardsearch", {
            theme: "facebook"
        });
        $("#store").tokenInput("/api/storesearch", {
            theme: "facebook"
        });
    });
    </script>

    <script>
        $('#shareme').sharrre({
            share: {
                googlePlus: true
                ,facebook: true
                ,twitter: true
                ,delicious: true
            },
            urlCurl: "http://cardmaster.lkl.kr/static/js/sharrre/sharrre.php",
            buttons: {
                googlePlus: {size: 'tall'},
                facebook: {layout: 'box_count'},
                twitter: {count: 'vertical'},
                delicious: {size: 'tall'}
            },
            hover: function(api, options){
                $(api.element).find('.buttons').show();
            }
        });
    </script>
    <title>Card Master</title>
</head>
<body class="modern-ui" onload="prettyPrint()">
    <div class="page">
        <div class="page-header">
            <div class="page-header-content">
                <h1>혜택정보 추가<small>_</small></h1>
            </div>
        </div>

        <div class="page-region">
            <div class="page-region-content">
                <div class="grid"><form method="post" action="/benefit/new">
                    <div class="row">
                        <div class="span10">
                            <h2>혜택 내용</h2>
                            <div class="input-control text span9">
                              <input name="description" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <div class="input-control select span2">
                              <select name="type">
                                <option value="discount">할인</option>
                                <option value="point">적립</option>
                              </select>
                            </div>
                            <div class="input-control text span4">
                              <input name="value" type="text" />
                            </div>
                            <label class="input-control radio">
                                <input name="value_type" value="rate" type="radio"  checked=""/>
                                <span class="helper">%</span>
                            </label>

                            <label class="input-control radio">
                                <input name="value_type" value="price" type="radio" />
                                <span class="helper">원</span>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>카드</h2>
                              <input id="card" name="card" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="span10">
                            <h2>가맹점</h2>
                              <input id="store" name="store" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <input type="submit" value="등록">
                        </div>
                    </div>

                </form></div>

            </div>
        </div>
    </div>



    </body>
</html>


