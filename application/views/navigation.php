<div class="page">
<div class="nav-bar">
    <div class="nav-bar-inner padding10">
        <span class="pull-menu"></span>

        <a href="/"><span class="element brand">
            <i class="icon-credit-card"></i>
            Card Master <small><?= include("version.phtml")?></small>
        </span></a>

        <div class="divider"></div>

        <ul class="menu">
            <li><a href="/">홈</a></li>
            <li data-role="dropdown">
                <a href="#">카드</a>
                <ul class="dropdown-menu">
                    <!--li><a href="/card">통계</a></li>
                    <li class="divider"></li-->
                    <li><a href="/card/search">검색</a></li>
                    <li><a href="/card/new">추가</a></li>
                </ul>
            </li>
            <li data-role="dropdown">
                <a href="#">가맹점</a>
                <ul class="dropdown-menu">
                    <!--li><a href="/store">통계</a></li>
                    <li class="divider"></li-->
                    <li><a href="/store/search">검색</a></li>
                    <li><a href="/store/new">추가</a></li>
                </ul>
            </li>
            <!--li data-role="dropdown">
                <a href="#">추천</a>
                <ul class="dropdown-menu">
                    <li><a href="/recommend">성별</a></li>
                    <li><a href="/recommend">나이</a></li>
                    <li class="divider"></li>
                    <li><a href="/recommend">사용 패턴</a></li>
                </ul>
            </li-->
<?
if($this->session->userdata('name')) {
?>
            <li data-role="dropdown">
                <a href="#"><?=$this->session->userdata('name')?>님</a>
                <ul class="dropdown-menu">
                    <li><a href="/user/card">내 카드</a></li>
                    <li class="divider"></li>
                    <li><a href="/user/logout">로그아웃</a></li>
                </ul>
            </li>
<?
} else {
?>
            <li><a href="/user/login">로그인</a></li>
<?
}
?>
        </ul>

    </div>
</div>
</div>
