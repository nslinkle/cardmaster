<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cardmodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function getCardNum() {
    return $this->db->get('card')->num_rows();
  }
  
  function newCard($input) {
    $cardInfo = array();
    $cardInfo['name'] = $input['name'];
    $cardInfo['description'] = $input['description'];
    $cardInfo['image'] = $input['image'];
    $cardInfo['card_type_id'] = $input['type'];
    $this->db->insert('card', $cardInfo);

    if(isset($input['brand'])) {
      $row = $this->db->get_where('card', $cardInfo)->last_row('array');
      $card_id = $row['card_id'];
      foreach($input['brand'] as $k => $v){
        $this->newCardBrandRel($card_id, $k);
      }
    }
  }

  function getCard($id) {
    $q = $this->db->query("SELECT `card`.*, `card_type`.`name` as `type`
    FROM `card`, `card_type`
    WHERE `card`.`card_type_id` = `card_type`.`card_type_id`
    AND `card`.`card_id` = '$id'");
    return $q->result_array();
  }

  function searchCard($q) {
    $q = $this->db->escape_like_str($q);

    $q = $this->db->query("SELECT `card`.*, `card_type`.`name` as `type`
    FROM `card`, `card_type`
    WHERE `card`.`card_type_id` = `card_type`.`card_type_id`
      AND `card`.`name` LIKE '%$q%'
    ORDER BY  `card`.`card_id` DESC 
    ");
    return $q->result_array();
  }

  function searchCardForApi($q) {
    $q = $this->db->escape_like_str($q);

    $q = $this->db->query("SELECT `card`.`card_id` as `id`, `card`.`name` as `name`
    FROM `card`
    WHERE `card`.`name` LIKE '%$q%'");
    return $q->result_array();
  }

  function getCardList() {
    $q = $this->db->query('SELECT `card`.*, `card_type`.`name` as `type`
    FROM `card`, `card_type`
    WHERE `card`.`card_type_id` = `card_type`.`card_type_id`
    ORDER BY  `card`.`card_id` DESC 
    ');
    return $q->result_array();
  }

  function appendCardBrand($cardList) {
    foreach($cardList as $i => $card) {
      $id = $card['card_id'];
      $q = $this->db->query("SELECT `brand`.`card_brand_id` as `id`, `name`, `description`, `image`
      FROM `brand`, `card_brand` 
      WHERE `brand`.`card_brand_id` =  `card_brand`.`card_brand_id`
        AND `brand`.`card_id` = '$id'");
      $cardList[$i]['brand'] = $q->result_array();
    }
    return $cardList;
  }

  function newCardBrandRel($card_id, $brand_id) {
    $this->db->insert('brand', array('card_id'=>$card_id, 'card_brand_id'=>$brand_id));
  }

  function getCardTypeList() {
    $q = $this->db->get('card_type');
    return $q->result_array();
  }
  
  function getCardBrandList() {
    $q = $this->db->get('card_brand');
    return $q->result_array();
  }
}
