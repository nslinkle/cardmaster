<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Benefitmodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function getBenefitNum() {
    return $this->db->get('benefit')->num_rows();
  }
  
  function newBenefit($input) {
    if(count($input['card'])==0 || count($input['store'])==0) return;
    $benefitInfo = array();
    $benefitInfo['description'] = $input['description'];
    $benefitInfo['rate'] = 1;
    $benefitInfo['price'] = 0;
    if($input['value_type']=='rate') $benefitInfo['rate'] = $input['value'];
    else if($input['value_type']=='price') $benefitInfo['price'] = $input['value'];
    $benefitInfo['type'] = $input['type'];

    $this->db->insert('benefit', $benefitInfo);

    $row = $this->db->get_where('benefit', $benefitInfo)->last_row('array');
    $benefit_id = $row['benefit_id'];
    foreach($input['card'] as $card_id){
      $this->db->insert('benefit_card', array('benefit_id'=>$benefit_id, 'card_id'=>$card_id));
    }
    foreach($input['store'] as $store_id){
      $this->db->insert('benefit_store', array('benefit_id'=>$benefit_id, 'store_id'=>$store_id));
    }
  }

  function getCardBenefit($card_id, $price=10000) {
    $q = $this->db->query("SELECT 
        `store`.`store_id`,
        `store`.`name` as `store_name`, 
        `store`.`image` as `store_image`, 
        `b`.`description` as `benefit_description`, 
        `b`.`type` as `benefit_type`, 
        `b`.`rate` as `benefit_rate`, 
        `b`.`price` as `benefit_price`, 
        `b`.`price`+(`b`.`rate`*$price) as `discount` 
    FROM `benefit` `b`, 
        `benefit_card` `bc`, 
        `benefit_store` `bs`, 
        `card`, `store`
    WHERE `b`.`benefit_id` = `bc`.`benefit_id`
      AND `b`.`benefit_id` = `bs`.`benefit_id`
      AND `bc`.`card_id` = `card`.`card_id`
      AND `bs`.`store_id` = `store`.`store_id`
      AND `bc`.`card_id` = '$card_id'
    ORDER BY  `discount` DESC");
    return $q->result_array();
  }
  function getStoreBenefit($store_id, $user_id=false, $price=10000) {
    $q = "SELECT 
        `card`.`card_id`, 
        `card`.`name` as `card_name`, 
        `card`.`image` as `card_image`, 
        `b`.`benefit_id` as `benefit_id`, 
        `b`.`description` as `benefit_description`, 
        `b`.`type` as `benefit_type`, 
        `b`.`rate` as `benefit_rate`, 
        `b`.`price` as `benefit_price`, 
        `b`.`price`+(`b`.`rate`*$price) as `discount` 
    FROM `benefit` `b`, 
        `benefit_card` `bc`, 
        `benefit_store` `bs`, 
        `card`, `store`
    WHERE `b`.`benefit_id` = `bc`.`benefit_id`
      AND `b`.`benefit_id` = `bs`.`benefit_id`
      AND `bc`.`card_id` = `card`.`card_id`
      AND `bs`.`store_id` = `store`.`store_id`
      AND `bs`.`store_id` = '$store_id'
    ORDER BY  `discount` DESC";
    if($user_id){
      $q = "SELECT 
          `card`.`card_id`, 
          `card`.`name` as `card_name`, 
          `card`.`image` as `card_image`, 
          `b`.`benefit_id` as `benefit_id`, 
          `b`.`description` as `benefit_description`, 
          `b`.`type` as `benefit_type`, 
          `b`.`rate` as `benefit_rate`, 
          `b`.`price` as `benefit_price`, 
          `b`.`price`+(`b`.`rate`*$price) as `discount` 
      FROM `benefit` `b`, 
          `benefit_card` `bc`, 
          `benefit_store` `bs`, 
          `card`, `store`, `owned`
      WHERE `b`.`benefit_id` = `bc`.`benefit_id`
        AND `b`.`benefit_id` = `bs`.`benefit_id`
        AND `bc`.`card_id` = `card`.`card_id`
        AND `bs`.`store_id` = `store`.`store_id`
        AND `bc`.`card_id` = `owned`.`card_id`
        AND `owned`.`user_id` = '$user_id'
        AND `owned`.`usable` = '1'
        AND `bs`.`store_id` = '$store_id'
      ORDER BY  `discount` DESC";
    }
    $q = $this->db->query($q);
    return $q->result_array();
  }
}
