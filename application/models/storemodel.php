<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Storemodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function getStoreNum() {
    return $this->db->get('store')->num_rows();
  }
  
  function newStore($input) {
    $storeInfo = array();
    $storeInfo['name'] = $input['name'];
    $storeInfo['description'] = $input['description'];
    $storeInfo['image'] = $input['image'];
    $storeInfo['store_benefit_type_id'] = $input['benefit_type'];
    $this->db->insert('store', $storeInfo);
  }

  function getStore($storeID) {
    if(is_numeric($storeID) == false) return null;

    $query = "SELECT " .
         "s.name as name, s.description as description, s.image as image, " .
         "s.store_id as id, t.name as type " .
         "FROM store s, store_benefit_type t WHERE " .
         "s.store_benefit_type_id = t.store_benefit_type_id AND " .
         "s.store_id = '$storeID';";

    $q = $this->db->query($query);

    return $q->result_array();
 }
 
  function getStoreList() {
    $query = "SELECT " .
         "s.name as name, s.description as description, s.image as image, " .
         "s.store_id as id, t.name as type FROM store s, store_benefit_type t " .
         "WHERE s.store_benefit_type_id = t.store_benefit_type_id;";

    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function searchStoreForApi($q) {
    $q = $this->db->escape_like_str($q);

    $q = $this->db->query("SELECT `store`.`store_id` as `id`, `store`.`name` as `name`
    FROM `store`
    WHERE `store`.`name` LIKE '%$q%'");
    return $q->result_array();
  }

  function getStoreListByName($name) {
    $name = "'%".$this->db->escape_like_str($name)."%'";

    $query = "SELECT " .
         "s.name as name, s.description as description, s.image as image, t.name as type, " .
         "s.store_id as id FROM store s, store_benefit_type t WHERE " .
         "s.store_benefit_type_id = t.store_benefit_type_id AND " .
         "s.name LIKE $name;";

    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function getStoreListByTypeID($typeID) {
    return $this->getStoreListByNameAndTypeID("", $typeID);
  }

  function getStoreListByNameAndTypeID($name, $typeID) {
    $name = "'%".$this->db->escape_like_str($name)."%'";

    $query = "SELECT " .
         "s.name as name, s.description as description, s.image as image, t.name as type, " .
         "s.store_id as id FROM store s, store_benefit_type t WHERE " .
         "s.store_benefit_type_id = t.store_benefit_type_id AND " .
         "s.store_benefit_type_id = '$typeID' AND " .
         "s.name LIKE $name;";

    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function getStoreBenefitTypeList() {
    $q = $this->db->get('store_benefit_type');
    return $q->result_array();
  }
}
