<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usemodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function getTimeDim() {
    $data = array(
                  'y'=>date('Y'),
                  'm'=>date('m'),
                  'd'=>date('d'),
                  'hh'=>date('H'),
                  'mm'=>date('i'),
                  'n'=>date('N'),
                  );
    $q = $this->db->get_where('time_dim', $data);

    $result = $q->result_array();
    if(count($result)){
      return $result[0]['time_id'];
    }else{
      $this->db->insert('time_dim', $data);
      $q = $this->db->get_where('time_dim', $data);

      $result = $q->result_array();
      return $result[0]['time_id'];
    }
  }
  function addUseInfo($user, $card, $benefit, $store, $amount) {
    $data = array(
      'time_id'=>$this->getTimeDim(),
      'user_id'=>$user,
      'card_id'=>$card, 
      'benefit_id'=>$benefit,
      'store_id'=>$store,
      'amount'=>$amount
    );
    $this->db->insert('use_info', $data);
  }
  function recCard($user_id){
    $y=date('Y');
    $m=date('m');
    $query = "
    SELECT `card`.*, SUM(`score`), SUM(`td`) as `td` FROM (
        SELECT 1, `ui`.* ,`amount` as `score`, `ui`.`amount` as `td`
        FROM `use_info` `ui`, `time_dim` `t` 
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
      UNION
        SELECT 2, `ui`.* ,`amount`*0.5 as `score`, 0
        FROM `use_info` `ui`, `time_dim` `t`, `user` `me`, `user` `u`
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
          AND `me`.`user_id` = '$user_id'
          AND `me`.`age` = `u`.`age`
          AND `ui`.`user_id` = `u`.`user_id`
      UNION
        SELECT 3, `ui`.* ,`amount`*0.5 as `score`, 0
        FROM `use_info` `ui`, `time_dim` `t`, `user` `me`, `user` `u`
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
          AND `me`.`user_id` = '$user_id'
          AND `me`.`gender` = `u`.`gender`
          AND `ui`.`user_id` = `u`.`user_id`
    ) AS `score`, `card`
    WHERE `card`.`card_id` = `score`.`card_id`
    GROUP BY `card_id`
    LIMIT 0 , 5
    ";
    $q = $this->db->query($query);

    $result = $q->result_array();
    return $result;
  }
  function recStore($user_id){
    $y=date('Y');
    $m=date('m');
    $query = "
    SELECT `store`.*, SUM(`score`), SUM(`td`) as `td` FROM (
        SELECT 1, `ui`.* ,`amount` as `score`, `ui`.`amount` as `td`
        FROM `use_info` `ui`, `time_dim` `t` 
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
      UNION
        SELECT 2, `ui`.* ,`amount`*0.5 as `score`, 0
        FROM `use_info` `ui`, `time_dim` `t`, `user` `me`, `user` `u`
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
          AND `me`.`user_id` = '$user_id'
          AND `me`.`age` = `u`.`age`
          AND `ui`.`user_id` = `u`.`user_id`
      UNION
        SELECT 3, `ui`.* ,`amount`*0.5 as `score`, 0
        FROM `use_info` `ui`, `time_dim` `t`, `user` `me`, `user` `u`
        WHERE `ui`.`time_id`=`t`.`time_id` 
          AND `t`.`y`='$y'
          AND `t`.`m`='$m'
          AND `me`.`user_id` = '$user_id'
          AND `me`.`gender` = `u`.`gender`
          AND `ui`.`user_id` = `u`.`user_id`
    ) AS `score`, `store`
    WHERE `store`.`store_id` = `score`.`store_id`
    GROUP BY `store_id`
    LIMIT 0 , 5
    ";
    $q = $this->db->query($query);

    $result = $q->result_array();
    return $result;
  }
}
