<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function getUserNum() {
    return $this->db->get('user')->num_rows();
  }

  function getEmailNum() {
    return $this->db->get('login_email')->num_rows();
  }

  function getFacebookNum() {
    return $this->db->get('login_facebook')->num_rows();
  }

  function newUserWithFacebook($uid, $at, $name, $gender, $age) {
    $input['uid'] = $this->db->escape_str($uid);
    $input['ac'] = $this->db->escape_str($at);
    $input['gender'] = $this->db->escape_str($gender);

    $userInfo = array();
    $userInfo['name'] = $name;
    $userInfo['age'] = $age;
    $userInfo['gender'] = $gender;

    $this->db->insert('user', $userInfo);

    $query = "SELECT user_id FROM user 
              WHERE user.name = '$name' 
                AND user.age = '$age'
                AND user.gender = '$gender';";

    $q = $this->db->query($query);
    $row = $q->last_row('array');

    $login_facebookInfo = array();
    $login_facebookInfo['facebook_uid'] = $uid;
    $login_facebookInfo['facebook_at'] = $at;
    $login_facebookInfo['user_id'] = $row['user_id'];

    $this->db->insert('login_facebook', $login_facebookInfo);
  }

  function newUserWithEmail($input) {
    $input['name'] = $this->db->escape_str($input['name']);
    $input['email'] = $this->db->escape_str($input['email']);

    if(isset($input['gender_male'])) $input['gender'] = "male";
    if(isset($input['gender_female'])) $input['gender'] = "female";

    $query = "SELECT * FROM login_email WHERE email = '".$input['email']."';";
    $q = $this->db->query($query);
    if($q) return false;

    $userInfo = array();
    $userInfo['name'] = $input['name'];
    $userInfo['age'] = $input['age'];
    $userInfo['gender'] = $input['gender'];

    $this->db->insert('user', $userInfo);

    $query = "SELECT user_id FROM user WHERE " .
         "user.name = '".$input['name']."' AND " .
         "user.age = '".$input['age']."' AND " .
         "user.gender = '".$input['gender']."';";

    $q = $this->db->query($query);
    $row = $q->last_row('array');

    $login_emailInfo = array();
    $login_emailInfo['email'] = $input['email'];
    $login_emailInfo['password'] = md5($input['password']);
    $login_emailInfo['validation'] = 1;
    $login_emailInfo['user_id'] = $row['user_id'];

    $this->db->insert('login_email', $login_emailInfo);
  }

  function getUser($userID) {
    if(is_numeric($userID) == false) return null;

    $query = "SELECT * FROM user WHERE user_id = '$userID';";
    $q = $this->db->query($query);

    return $q->result_array();
  }

  function getUserList() {
    $query = "SELECT * FROM user;";
    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function tryLoginWithEmail($email, $password) {
    $email = $this->db->escape_like_str($email);
    $password = md5($this->db->escape_like_str($password));

    $query = "SELECT " .
         "e.email as email, u.user_id as user_id, u.name as name, u.age as age, u.gender as gender " .
         "FROM user u, login_email e WHERE " .
         "e.user_id = u.user_id AND " .
         "e.email = '$email' AND e.password = '$password';";

    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function tryLoginWithFacebook($facebook_uid) {
    $facebook_uid = $this->db->escape_like_str($facebook_uid);

    $query = "SELECT " .
         "u.user_id, u.name as name, u.age as age, u.gender as gender " .
         "FROM user u, login_facebook f WHERE " .
         "f.user_id = u.user_id AND " .
         "f.facebook_uid = '$facebook_uid';";

    $q = $this->db->query($query); 

    return $q->result_array();
  }

  function addUserCard($user_id, $card_id) {
    $data = array('user_id'=>$user_id, 'card_id'=>$card_id);
    $q = $this->db->get_where('owned', $data);
    if(count($q->result_array())==0){
      $this->db->insert('owned', $data);
    }
  }

  function setUserCardUsable($user_id, $card_id, $usable) {
    $this->db->where(array('user_id'=>$user_id, 'card_id'=>$card_id));
    $this->db->update('owned', array('usable'=>$usable));
  }

  function deleteUserCard($user_id, $card_id) {
    $this->db->where(array('user_id'=>$user_id, 'card_id'=>$card_id));
    $this->db->delete('owned');
  }

  function getUserCardList($user_id) {
    $query = "SELECT `card`.*, `card_type`.`name` as `type`, `owned`.`usable`
              FROM `owned`, `user`, `card`, `card_type`
              WHERE `owned`.`user_id`=`user`.`user_id`
              AND `card`.`card_type_id` = `card_type`.`card_type_id`
              AND `owned`.`card_id`=`card`.`card_id` 
              AND `owned`.`user_id` = '$user_id'";
    $q = $this->db->query($query); 
    return $q->result_array();
  }
}

