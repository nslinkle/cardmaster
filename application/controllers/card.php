<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Card extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('cardmodel');
  }

  // URL : /card
	public function index() {
		$this->load->view('welcome_message');
	}

  // URL : /card/{card_id}
	public function info($card_no) {
    $this->load->model('benefitmodel');
    $viewResult = array();
    $viewResult['card_info'] = $this->cardmodel->getCard($card_no);
    $viewResult['card_info'] = $this->cardmodel->appendCardBrand($viewResult['card_info']);
    $viewResult['card_info'] = $viewResult['card_info'][0];
    $viewResult['card_benefit'] = $this->benefitmodel->getCardBenefit($card_no);
		$this->load->view('card_info', $viewResult);
	}

  // URL : /card/new
	public function newcard() {
    $post=$this->input->post();
    if($post){$this->_newcard($post);return;}
    $viewResult = array();
    $viewResult['card_type'] = $this->cardmodel->getCardTypeList();
    $viewResult['card_brand'] = $this->cardmodel->getCardBrandList();
		$this->load->view('card_new', $viewResult);
	}
	public function _newcard($data) {
    $data['image'] = null;

    $config['upload_path'] = 'static/upload/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '300';
    $config['max_width']  = '640';
    $config['max_height']  = '480';
    $config['encrypt_name']  = true;
    $this->load->library('upload', $config);
    if($this->upload->do_upload('image')) {
      $file_info = $this->upload->data();
      $data['image'] = $file_info['file_name'];
    }
    $this->cardmodel->newCard($data);
    redirect('/card/search', 'refresh');
  }

  // URL : /card/search
	public function search() {
    $post=$this->input->post();
    if($post){$this->_search($post);return;}

    $viewResult = array();
    $viewResult['card_list'] = $this->cardmodel->getCardList();
    $viewResult['card_list'] = $this->cardmodel->appendCardBrand($viewResult['card_list']);
		$this->load->view('card_search', $viewResult);
	}

	public function _search($data) {
    if(!isset($data['q'])){$data['q']='';}

    $viewResult = array();
    $viewResult['card_list'] = $this->cardmodel->searchCard($data['q']);
    $viewResult['card_list'] = $this->cardmodel->appendCardBrand($viewResult['card_list']);
		$this->load->view('card_search', $viewResult);
	}
}

