<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

  // URL : /
	public function index() {
    $this->load->model('usermodel');
    $this->load->model('cardmodel');
    $this->load->model('storemodel');
    $this->load->model('benefitmodel');
    $this->load->model('usemodel');

		$user_id = $this->session->userdata('user_id');

    $viewResult = array();
    $viewResult['user_num'] = $this->usermodel->getUserNum();
    $viewResult['user_email_num'] = $this->usermodel->getEmailNum();
    $viewResult['user_facebook_num'] = $this->usermodel->getFacebookNum();
    $viewResult['user_num'] = $this->usermodel->getUserNum();
    $viewResult['card_num'] = $this->cardmodel->getCardNum();
    $viewResult['store_num'] = $this->storemodel->getStoreNum();
    $viewResult['benefit_num'] = $this->benefitmodel->getBenefitNum();
    if($user_id) {
      $viewResult['rec_card'] = $this->usemodel->recCard($user_id);
      $viewResult['rec_store'] = $this->usemodel->recStore($user_id);
    }
		$this->load->view('main', $viewResult);
	}

}

