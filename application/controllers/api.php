<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
  function __construct() {
    parent::__construct();
  }

  // URL : /api/storesearch
	public function storesearch() {
    $this->load->model('storemodel');
    $json_response = json_encode($this->storemodel->searchStoreForApi($this->input->get('q')));

    if($this->input->get('callback')) {
        $json_response = $this->input->get('callback') . "(" . $json_response . ")";
    }

    echo $json_response;
	}

  // URL : /api/cardsearch
	public function cardsearch() {
    $this->load->model('cardmodel');
    $json_response = json_encode($this->cardmodel->searchCardForApi($this->input->get('q')));

    if($this->input->get('callback')) {
        $json_response = $this->input->get('callback') . "(" . $json_response . ")";
    }

    echo $json_response;
	}
}

