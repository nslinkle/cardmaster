<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Benefit extends CI_Controller {

  // URL : /benefit
	public function index() {
		$this->load->view('welcome_message');
	}

  // URL : /benefit/new
	public function newbenefit() {
    $post = $this->input->post();
    if($post){$this->_newbenefit($post);return;}

		$this->load->view('benefit_new');
	}
	public function _newbenefit($data) {
    if(!$data['description']) exit();
    if(!is_numeric($data['value'])) exit();
    if($data['type']=='discount') $data['type']=1;
    else if($data['type']=='point') $data['type']=2;
    else exit();
    if($data['value_type']=='rate') $data['value']=$data['value']/100;
    else if($data['value_type']=='price') {} else exit();
    $data['card'] = explode(',', $data['card']);
    $data['store'] = explode(',', $data['store']);
    $cardArray = array();
    foreach($data['card'] as $id){
      if(is_numeric($id)) $cardArray[] = $id;
    }
    $storeArray = array();
    foreach($data['store'] as $id){
      if(is_numeric($id)) $storeArray[] = $id;
    }
    if(count($cardArray)==0) exit();
    if(count($storeArray)==0) exit();
    $data['card'] = $cardArray;
    $data['store'] = $storeArray;

    $this->load->model('benefitmodel');
    $this->benefitmodel->newBenefit($data);
    echo 'OK!';
  }

  // URL : /benefit/cardnew
	public function cardnew($card_id) {
		$this->load->view('benefit_new');
	}

  // URL : /benefit/storenew
	public function storenew($store_id) {
		$this->load->view('benefit_new');
	}
}

