<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('storemodel');
	}

  // URL : /store
	public function index() {
		$this->load->view('welcome_message');
	}

  // URL : /store/{card_id}
	public function info($store_no) {
    $post=$this->input->post();
    if($post){$this->_info($store_no, $post);return;}

		$user_id = $this->session->userdata('user_id');
		$viewResult = array();
		$viewResult['store'] = $this->storemodel->getStore($store_no);
		$viewResult['result'] = ($viewResult['store'] != null);

		if($viewResult['store']) $viewResult['store'] = $viewResult['store'][0];

    $this->load->model('benefitmodel');
    $viewResult['store_benefit'] = $this->benefitmodel->getStoreBenefit($store_no, $user_id);

    $viewResult['benefit_price'] = 10000;
		$this->load->view('store_info', $viewResult);
	}
	public function _info($store_no, $data) {
    if(!(isset($data['price']) && is_numeric($data['price']))){$data['price']=10000;}

		$user_id = $this->session->userdata('user_id');
		$viewResult = array();
		$viewResult['store'] = $this->storemodel->getStore($store_no);
		$viewResult['result'] = ($viewResult['store'] != null);

		if($viewResult['store']) $viewResult['store'] = $viewResult['store'][0];

    $this->load->model('benefitmodel');
    $viewResult['store_benefit'] = $this->benefitmodel->getStoreBenefit($store_no, $user_id, $data['price']);

    $viewResult['benefit_price'] = $data['price'];

    if(count($viewResult['store_benefit'])) {
      $this->load->model('usemodel');
      $this->usemodel->addUseInfo($user_id, $viewResult['store_benefit'][0]['card_id'], $viewResult['store_benefit'][0]['benefit_id'], $store_no, $viewResult['store_benefit'][0]['discount']);
    }
		$this->load->view('store_info', $viewResult);
  }

  // URL : /store/new
	public function newstore() {
		$post = $this->input->post();
		if($post) {
			$this->_newstore($post);
			return;
		}

		$this->load->model('storemodel');
		$viewResult = array();
		$viewResult['store_benefit_type'] = $this->storemodel->getStoreBenefitTypeList();

		$this->load->view('store_new', $viewResult);
	}

	public function _newstore($data) {
		$data['image'] = "";

		$config['upload_path'] = 'static/upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '300';
		$config['max_width']  = '640';
		$config['max_height']  = '480';
		$config['encrypt_name']  = true;
		$this->load->library('upload', $config);

		if($this->upload->do_upload('image')) {
			$file_info = $this->upload->data();
			$data['image'] = $file_info['file_name'];
		}

		$this->storemodel->newStore($data);

		redirect('/store/search', 'refresh');
	}

  // URL : /store/search
	public function search() {
		$post = $this->input->post();
		$get = $this->input->get();

		$viewResult = array();
		if($get && isset($get['typeid'])) $typeID = $get['typeid'];
		else $typeID = 0;

		if($post && $post['query'] != "") {
			if($post['search_type'] == "name") {
				$viewResult['query'] = $post['query'];

				if($typeID == 0)
					$viewResult['store_list'] = $this->storemodel->getStoreListByName($post['query']);
				else
					$viewResult['store_list'] = $this->storemodel->getStoreListByNameAndTypeID($post['query'], $typeID);
			}
		} else {
			if($typeID != 0)
				$viewResult['store_list'] = $this->storemodel->getStoreListByTypeID($typeID);
			else
				$viewResult['store_list'] = $this->storemodel->getStoreList();
		}

		$viewResult['store_benefit_type_list'] = $this->storemodel->getStoreBenefitTypeList();
		$viewResult['typeid'] = $typeID;

		$this->load->view('store_search', $viewResult);
	}
}

