<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('usermodel');
	}

  // URL : /user
	public function index() {
		$this->load->view('user');
	}

	public function card() {
		$user_id = $this->session->userdata('user_id');

		$viewResult['card_list'] = $this->usermodel->getUserCardList($user_id);
		$this->load->model('cardmodel');
    $viewResult['card_list'] = $this->cardmodel->appendCardBrand($viewResult['card_list']);
		$this->load->view('user_card', $viewResult);
	}

	public function newcard($card_id) {
		$user_id = $this->session->userdata('user_id');

		$this->usermodel->addUserCard($user_id, $card_id);
		redirect('/user/card', "refresh");
	}

	public function cardusable($card_id, $usable) {
		$user_id = $this->session->userdata('user_id');

		$this->usermodel->setUserCardUsable($user_id, $card_id, $usable);
		redirect('/user/card', "refresh");
	}

	public function carddelete($card_id) {
		$user_id = $this->session->userdata('user_id');

		$this->usermodel->deleteUserCard($user_id, $card_id);
		redirect('/user/card', "refresh");
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect("/", "refresh");
	}

	public function login() {
		$post = $this->input->post();
		$get = $this->input->get();
		if($post) {
			$this->_login($post);
			return;
		}
	
		$message = array();

		if($get && isset($get['signup']) && $get['signup'] == "1")
			$this->load->view('user_login', array('result' => true));
		else
			$this->load->view('user_login');
	}

	public function _login($data) {
		$message = array();
		$viewResult = array();

		if(!isset($data['email']) || $data['email'] == "")
			$message[] = "이메일을 확인해주세요.";

		if(!isset($data['password']) || $data['password'] == "")
			$message[] = "비밀번호를 확인해주세요.";

		if(count($message) > 0) {
			$viewResult['result'] = false;
			$viewResult['message'] = $message;

			$this->load->view('user_login', $viewResult);
			return;
		}

		$userInfo = $this->usermodel->tryLoginWithEmail($data['email'], $data['password']);

		if($userInfo) {
			$this->session->set_userdata($userInfo[0]);
			redirect("/", "refresh");
		} else {
			$message[] = "아이디와 비밀번호를 확인해주세요.";
			$viewResult['result'] = false;
			$viewResult['message'] = $message;

			$this->load->view('user_login', $viewResult);
			return;
		}
	}

	public function signup() {
		$post = $this->input->post();
		if($post) {
			$this->_signup($post);
			return;
		}

		$this->load->view('user_signup');
	}

	public function _signup($data) {
		$message = array();

		if(!isset($data['email']) || $data['email'] == "")
			$message[] = "이메일을 확인해주세요.";

		if(!isset($data['password']) || !isset($data['password2']) ||
			  $data['password'] == "" || $data['password'] != $data['password2'])
			$message[] = "비밀번호를 확인해주세요.";

		if(!isset($data['name']) || $data['name'] == "")
			$message[] = "이름을 확인해주세요.";

		if(!isset($data['age']) || $data['age'] == "" || !is_numeric($data['age']))
			$message[] = "나이을 확인해주세요.";

		if(!isset($data['gender_male']) && !isset($data['gender_female']))
			$message[] = "성별을 확인해주세요.";

		if(!$this->usermodel->newUserWithEmail($data))
			$message[] = "이미 등록된 이메일입니다.";

		if(count($message) > 0) {
			$this->load->view('user_signup', array('message' => $message));
			return;
		}

		redirect("/user/login?signup=1", "refresh");
	}

}

