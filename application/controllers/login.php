<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  // URL : /login
	public function index() {
		$this->load->view('welcome_message');
	}

  // URL : /login/email
	public function email() {
		$this->load->view('welcome_message');
  }
	
  // URL : /login/facebook
  public function facebook() {
		$this->load->library('session');
    $this->load->library('facebook');

    $get_code = $this->input->get('code');
    if(!$get_code){
      $loginUrl = $this->facebook->getLoginUrl();
      redirect($loginUrl);
    }else if($get_code){
      $this->facebook->setRequest($this->input->get());
      $facebook_at = $this->facebook->getAccessToken();
      $facebook_uid = $this->facebook->getUser();
      if($facebook_uid==0){
        redirect('/login');
        return;
      }
      $this->load->model('usermodel');
      $user_info = $this->usermodel->tryLoginWithFacebook($facebook_uid);
      if(count($user_info)==0){
        $facebook_info = $this->facebook->api('/me');
        $name = $facebook_info['name'];
        $gender = $facebook_info['gender'];
        $birthday = explode('/', $facebook_info['birthday']);
        $age = date('Y') - $birthday[2] + 1;

        $this->usermodel->newUserWithFacebook($facebook_uid, $facebook_at, $name, $gender, $age);
        $user_info = $this->usermodel->tryLoginWithFacebook($facebook_uid);
      }
      if(!$user_info){
        redirect('/');
        return;
      }
      //$user_no = $user_info['no'];
      //$this->user_m->setFacebookAt($user_no, $facebook_at);

      $session_data = $user_info[0];

      $this->session->set_userdata($session_data);
      //some browser has bug set_cookie with redirect(302)
      redirect('/main');
      return;
    }
  }
}

