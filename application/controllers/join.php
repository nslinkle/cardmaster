<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Join extends CI_Controller {

  // URL : /join
	public function index() {
		$this->load->view('welcome_message');
	}

  // URL : /join/email
	public function email() {
		$this->load->view('welcome_message');
	}

  // URL : /join/facebook
	public function facebook() {
		$this->load->view('welcome_message');
	}
}

