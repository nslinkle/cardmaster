<?php
	$query = "CREATE TABLE user(" .
        		"user_id int NOT NULL AUTO_INCREMENT, " .
        		"name VARCHAR(10) NOT NULL, " .
        		"age INT NOT NULL, " .
        		"gender VARCHAR(10) NOT NULL, " .
        		"CONSTRAINT pk_user PRIMARY KEY(user_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE login_email(" .
			"email VARCHAR(100) NOT NULL, " .
			"password VARCHAR(100) NOT NULL, " .
        		"validation BOOLEAN NOT NULL, " .
        		"user_id INT NOT NULL, " .
        		"CONSTRAINT fk_login_email FOREIGN KEY(user_id) REFERENCES user(user_id));";
	echo "$query\n\n";

        $query = "CREATE TABLE login_facebook(" .
			"facebook_uid VARCHAR(511) NOT NULL, " .
        		"access_code VARCHAR(511) NOT NULL, " .
        		"user_id INT NOT NULL, " .
        		"CONSTRAINT fk_login_facebook FOREIGN KEY(user_id) REFERENCES user(user_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE card_type(" .
			"card_type_id INT NOT NULL AUTO_INCREMENT, " .
			"name VARCHAR(100) NOT NULL, " .
			"description VARCHAR(1023) NOT NULL, " .
			"constraint pk_card_type primary key(card_type_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE card(" .
			"card_id int NOT NULL AUTO_INCREMENT, " .
			"name VARCHAR(127) NOT NULL, " .
			"image VARCHAR(511) NOT NULL, " .
        		"description VARCHAR(1023) NOT NULL, " .
        		"annual_fee INT NOT NULL, " .
			"card_type_id int not null, " .
			"constraint pk_card primary key(card_id), " .
			"constraint fk_card foreign key(card_type_id) references card_type(card_type_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE card_brand(" .
			"card_brand_id INT NOT NULL AUTO_INCREMENT, " .
			"name VARCHAR(100) NOT NULL, " .
			"description VARCHAR(1023) NOT NULL, " .
			"constraint pk_card_type primary key(card_brand_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE brand(" .
			"card_id int not null, " .
			"card_brand_id int not null, " .
			"image varchar(511) not null, " .
			"constraint fk_type1 foreign key(card_id) references card(card_id), " .
			"constraint fk_type2 foreign key(card_brand_id) references card_brand(card_brand_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE card_state(" .
			"usable boolean not null, " .
			"point int not null, " .
			"user_id int not null, " .
			"card_id int not null, " .
			"constraint fk_card_state1 foreign key(user_id) references user(user_id), " .
			"constraint fk_card_state2 foreign key(card_id) references card(card_id));";
	echo "$query\n\n";

	$query = "create table store(" .
			"store_id int not null auto_increment, " .
			"name varchar(100) not null, " .
			"description varchar(1023) not null, " .
			"image varchar(511) not null, " .
			"constraint pk_store primary key(store_id));";
	echo "$query\n\n";

	$query = "create table benefit_type(" .
			"benefit_type_id int not null auto_increment, " .
			"name varchar(100) not null, " .
			"description varchar(1023) not null, " .
			"constraint pk_benefit_type primary key(benefit_type_id));";
	echo "$query\n\n";

	$query = "CREATE TABLE benefit(" .
			"benefit_id int not null auto_increment, " .
			"title varchar(511) not null, " .
			"discount_price int not null, " .
			"discount_rate float not null, " .
			"point_price int not null, " .
			"point_rate int not null, " .
			"benefit_type_id int not null, " .
			"card_id int not null, " .
			"store_id int not null, " .
			"constraint pk_benefit primary key(benefit_id), " .
			"constraint fk_benefit1 foreign key(benefit_type_id) references benefit_type(benefit_type_id), " .
			"constraint fk_benefit2 foreign key(card_id) references card(card_id), " .
			"constraint fk_benefit3 foreign key(store_id) references store(store_id));";
	echo "$query\n\n";

	$query = "create table benefit_time(" .
			"start_time int not null, " .
			"end_time int not null, " .
			"benefit_id int not null, " .
			"constraint fk_benefit_type foreign key(benefit_id) references benefit(benefit_id));";
	echo "$query\n\n";

	$query = "create table use_info(" .
			"time datetime not null, " .
			"amount int not null, " .
			"user_id int not null, " .
			"card_id int not null, " .
			"benefit_id int not null, " .
			"store_id int not null, " .
			"constraint fk_use_info1 foreign key(user_id) references user(user_id), " .
			"constraint fk_use_info2 foreign key(card_id) references card(card_id), " .
			"constraint fk_use_info3 foreign key(benefit_id) references benefit(benefit_id), " .
			"constraint fk_use_info4 foreign key(store_id) references store(store_id));";
	echo "$query\n\n";
?>