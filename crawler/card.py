#!/usr/bin/env python
#-*- coding: utf-8 -*-

import urllib
from bs4 import BeautifulSoup
import pymysql
import time

conn = pymysql.connect(host='127.0.0.1', port=3306, user='card', passwd='card', db='card', charset='utf8')

def geturl(url):
    f = urllib.urlopen(url)
    s = f.read()
    f.close()
    return s

def download(url, filename):
    fp = urllib.urlopen(url)
    op = open(filename, "wb")
    n = 0
    while 1:
        s = fp.read(8192)
        if not s:
            break
        op.write(s)
        n = n + len(s)
    fp.close()
    op.close()

def insertdb(name, desc, image):
    cur = conn.cursor()
    cur.execute("INSERT INTO `card`.`card` VALUES (NULL, "+pymysql.escape_string(name)+", '"+image+"', "+pymysql.escape_string(desc)+", '0', '1');")
    cur.close()
    
def getcard(card_id):
    print card_id
    soup = BeautifulSoup(geturl("http://card.search.naver.com/?where=service&anq=0&singleCardId=" + str(card_id)))
    if not soup.find(class_="detail"):
        return
    name = soup.find(class_="detail").find("span").text
    print name
    desc = soup.find(class_="detail").find("dl").find("dd").text
    print desc
    image = soup.find(class_="thmb").find("img")['src']
    print image
    image_name = image.split("/")[-1].split("%2F")[-1]
    print image_name
    download(image, image_name)
    insertdb(name, desc, image_name)

for i in range(182, 10000):
    getcard(i)
    time.sleep(1)

conn.close()



